var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)
function arrayToObject(arr) {
    let fullname="";
    let people={};
    let n=1;
    if(arr.length===0||arr==null){
        people={error:"please input data"}
    }
    for(let i=0;i<arr.length;i++){
        fullname=arr[i][0]+" "+arr[i][1];
        people[fullname]={
            firstName:arr[i][0],
                lastName:arr[i][1],
                gender:arr[i][2],
                age:arr[i][3]<=thisYear && arr[i][3]>=0 ? thisYear-arr[i][3]:"Invalid Birth Year"
        }
        
        
    }
    for (let key in people){
        if(people.hasOwnProperty(key)){
        if(key!=="error"){
          console.log(`${n}. ${key} : `,people[key])
          console.log("\n");
        }else{
            console.log(`${key} : `,people[key])
            console.log("\n");
        }
          
        }
        n++;
     }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log("\n");

//Number 2

let shoplist={
"Sepatu brand Stacattu" :1500000,
"Baju brand Zoro":500000,
"Baju brand H&N" :250000,
"Sweater brand Uniklooh" : 175000,
"Casing Handphone":50000
}

function shoppingTime(memberId, money) {
    // you can only write your code here!
   let listPurchased=[];
   let change=money;
    if(memberId==null||memberId==""){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }else if(money<50000){
        return "Mohon maaf, uang tidak cukup"
    }else {
        
    for (let key in shoplist){
        if(change>=shoplist[key]){
            change=change-shoplist[key];
            listPurchased.push(key);
        }else{
            change-=0;
        }
    }
    let Billing={
        memberId:memberId,
        money:money,
        listPurchased:listPurchased,
        changeMoney:change
    }
    return Billing;
}
    
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  //No Soal No. 3 (Naik Angkot)

  console.log("");
  
  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let penumpang=[];
    //your code here
    if(arrPenumpang.length>0){
        for(let i=0;i<arrPenumpang.length;i++){
            let bayar=(rute.indexOf(arrPenumpang[i][2])-rute.indexOf(arrPenumpang[i][1]))*2000;
            penumpang.push({penumpang:arrPenumpang[i][0],naikDari:arrPenumpang[i][1],tujuan:arrPenumpang[i][2],bayar:bayar})
        }
        return penumpang;
    }else return [];
    
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]