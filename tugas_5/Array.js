//Soal No. 1 (Range)

function range(a, b) {
  let array = [];
  let n = 0;
  if (a <= b) {
    for (let i = a; i <= b; i++) {
      array[n] = i;
      n++;
    }
  } else if (a >= b) {
    for (let j = a; j >= b; j--) {
      array[n] = j;
      n++;
    }
  } else {
    array = -1;
  }
  return array;
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

//Soal No. 2 (Range with Step)
console.log("\n");

function rangeWithStep(a, b, step) {
  let array = [];
  let n = 0;
  if (a <= b) {
    for (let i = a; i <= b; i += step) {
      array[n] = i;
      n++;
    }
  } else if (a >= b) {
    for (let j = a; j >= b; j -= step) {
      array[n] = j;
      n++;
    }
  } else {
    array = -1;
  }
  return array;
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

//Soal No. 3 (Sum of Range)
console.log("\n");

function sum(a, b, step) {
  let sum = 0;

  if (a == null) {
    sum = 0;
  } else if (b == null) {
    sum += a;
  } else if (step == null) {
    let array = rangeWithStep(a, b, 1);
    for (let i = 0; i < array.length; i++) {
      sum += array[i];
    }
  } else {
    let array = rangeWithStep(a, b, step);
    for (let i = 0; i < array.length; i++) {
      sum += array[i];
    }
  }
  return sum;
}

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

console.log("\n");

//Soal No. 4 (Array Multidimensi)

//contoh input
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

function dataHandling(input) {
  for (let i = 0; i < input.length; i++) {
    console.log(
      "Nomor ID : " +
        input[i][0] +
        "\nNama Lengkap : " +
        input[i][1] +
        "\nTTL : " +
        input[i][2] +
        " " +
        input[i][3] +
        "\nHobi : " +
        input[i][4]
    );
    console.log("\n");
  }
}
dataHandling(input);


//Soal No. 5 (Balik Kata)
console.log("\n");

function balikKata(word){
    let reverseword="";
    for(let i=word.length-1;i>=0;i--){
        reverseword+=word[i];
    }
    return reverseword;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


//Soal No. 6 (Metode Array)

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(input) {
    //splice
    input.splice(1,1,"Roman Alamsyah Elsharawy")
    input.splice(2,1,"Provinsi Bandar Lampung")
    input.splice(4,1,"Pria","SMA Internasional Metro")
    console.log(input); 

    //Split
    let tanggal=input[3].split("/");
    switch (Number(tanggal[1])) {
        case 1:
          console.log("January");
          ;
          break;
        case 2:
          console.log("February");
          break;
        case 3:
          console.log("March");
          break;
        case 4:
          console.log("April");
          break;
        case 5:
          console.log("May");
          break;
        case 6:
          console.log("June");
          break;
        case 7:
          console.log("July");
          break;
        case 8:
          console.log("August");
          break;
        case 9:
          console.log("September");
          break;
        case 10:
          console.log("October");
          break;
        case 11:
          console.log("November");
          break;
        case 12:
          console.log("December");
          break;
        default:
            return console.log("Number of month is wrong");
          break;
      }

      //Sort
      
      console.log(tanggal.sort(function(a, b){return b-a}));

      //JOin
      console.log(tanggal.sort(function(a, b){return a-b}).join('-'));
      
     console.log((input[1].slice(0,15)));
      
}
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 