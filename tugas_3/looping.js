//No. 1 Looping While
let n=2;
console.log("LOOPING PERTAMA");
while (n<=20) {
    console.log(n+" - I love coding");
    n+=2;
}
console.log("LOOPING KEDUA");
let i=20;
while (n>=2) {
    console.log(n+" - I love coding");
    n-=2;
}

console.log("");

//No. 2 Looping menggunakan for
for(let j=1;j<=20;j++){
    if(j%3===0 && j%2===1){
        console.log(j+" - I Love Coding");
    }else if(j%2===0){
        console.log(j+" - Berkualitas");
    }else{
        console.log(j+" - Santai");
    }
}

console.log("");

//No. 3 Membuat Persegi Panjang #
let String="";
for(let panjang=1;panjang<=4;panjang++){
    for(let lebar=1;lebar<=8;lebar++){
        String+="#"
    }
    String+="\n"
}
console.log(String);

//No. 4 Membuat Tangga

let triangle="";
for(let t=1;t<=7;t++){
    for(let v=1;v<=t;v++){
        triangle+="* "
    }
    triangle+="\n"
}
console.log(triangle);

console.log("");

//No. 5 Membuat Papan Catur
let catur="";
for(let c=1;c<=8;c++){
    if(c%2==0){
        for(let i=1;i<=8;i+=2){
            catur+="# "
        }
    }else{
        for(let i=1;i<=8;i+=2){
           catur+=" #"
        }
    }
    catur+="\n"
}
console.log(catur);
