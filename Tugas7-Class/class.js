// Realese 2
class Animal {
    // Code class di sini
    constructor(name) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("\n");

//Realese 1
class Ape extends Animal {
    constructor(name) {
        super(name);
        this.legs = 2;
    }
    yell() {
        console.log("Auoooo");
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
    }
    jump() {
        console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
console.log(sungokong.legs);

//NO 2

// function Clock({ template }) {

//     var timer;

//     function render() {
//       var date = new Date();

//       var hours = date.getHours();
//       if (hours < 10) hours = '0' + hours;

//       var mins = date.getMinutes();
//       if (mins < 10) mins = '0' + mins;

//       var secs = date.getSeconds();
//       if (secs < 10) secs = '0' + secs;

//       var output = template
//         .replace('h', hours)
//         .replace('m', mins)
//         .replace('s', secs);

//       console.log(output);
//     }

//     this.stop = function() {
//       clearInterval(timer);
//     };

//     this.start = function() {
//       render();
//       timer = setInterval(render, 1000);
//     };

//   }
class Clock {
    
    constructor({template}) {
        this.template=template;
        this.timer="";
    }
    start() {
        let newRender=function re(){
            this.render();
        }.bind(this);
        this.timer = setInterval(newRender, 1000);
    };
    stop() {
        clearInterval(this.timer);
    };

    
    render(){
    let template=this.template;
    var date = new Date();
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
      console.log(output);
    }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start(); 
let stop=function st(){
    clock.stop();
}
setInterval(stop,6000);